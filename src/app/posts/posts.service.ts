import { Injectable } from '@angular/core';
import {Http} from '@angular/http'; // import http for pulling data from sever.
import 'rxjs/add/operator/map'; // map function allows us to change the format of the file from string to json. //inside we will use arrow notation.
import 'rxjs/add/operator/delay';
@Injectable()

export class PostsService {

private _url = 'http://jsonplaceholder.typicode.com/posts' ; //

constructor(private _http: Http) { } // declaring on a private attribute from type Http thst was imported. 


  getPosts(){

   return this._http.get(this._url).map( res => res.json()).delay(2000)    //res is just a name 
   //  import שהצהרנו עלייה בתוך הבנאי ועשינו   Http  בשורה למעלה השתמשנו בתכונה  
  //  ,  URL כדי לשלוף את המידע מתוך ה observable שמחזירה get לאחר מכן השתמשנו ב 
   // json כדי להפוך מ סטרינג ל  map נשתמש ב   observable לאחר מכן כדי לשנות את סוג הפורמט מה 
  }

}
