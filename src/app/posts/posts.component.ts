import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {



posts;
isloadnig:Boolean = true;

  constructor(private _postsService:PostsService) { } 
// decalring an object postservice from type postservice.
// to allow us to make changes in "ngOnInit". 

  deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post), 1
    )
  }
  showPost(post){
    this.posts
  }
addPost(post){
  this.posts.push(post)

}

  ngOnInit() {

     // left usersData its the input of the function.
      this._postsService.getPosts().subscribe(postsData => {this.posts = postsData ; this.isloadnig = false });
        // end of spinner   
      // subscribe: to pull the data from observable, inside there is arrow notation.
  }                                                                                 //  function(postdata) {
                                                                                    //  this.posts = postdata }
}
