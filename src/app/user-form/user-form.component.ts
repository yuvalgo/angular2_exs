import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {


 @Output() postAddedEvent = new EventEmitter<Post>();
  post:Post = {   // needs all the properties of the class.
    body: '',
    title: '',
    userId:''
  };



onSubmit(form:NgForm){   //the method sends the new post to parent (poata) by the emit function.
console.log(form);
this.postAddedEvent.emit(this.post); // pushes blank values to the input fields on each submit.
    this.post = {
       body: '',
       title: '',
       userId:''
}
}


  constructor() { }

  ngOnInit() {
  }

}



